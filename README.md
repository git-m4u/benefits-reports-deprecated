# dotz-report
Project created to generate dotz report

## Requirements
* python 3.8.x
* pip 20.0.x
* venv
* zip

## Build
```
$ sh build.sh
```

## Crendentials

* https://console.aws.amazon.com/secretsmanager/home?region=us-east-1#!/secret?name=datalake-redshit
