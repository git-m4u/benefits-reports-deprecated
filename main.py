import redshift_connector
import csv
import boto3
import json
from datetime import date
import dateutil.relativedelta
import calendar
import os

def get_bool_env(name: str) -> bool:
    return os.getenv(name, 'False').lower() in ('true', '1', 't')

# Adding checks because it was considering "false" to be true in if statements
DAILY_MODE = get_bool_env('DAILY_MODE')

SERVICES_IDS = os.environ['SERVICES_IDS']
BUCKET_NAME = os.environ['BUCKET_NAME']
DIR_NAME = 'mensal' if not DAILY_MODE else 'diario'

DB_HOST = 'bi-telco.connector.datalake.m4u.io'
DB_USER = 'benefit.platform'
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_DATABASE = 'master'

today = date.today()

if DAILY_MODE:
    today = today - dateutil.relativedelta.relativedelta(days=1)
    day = str(today.day).zfill(2)
    month = str(today.month).zfill(2)
    year = str(today.year).zfill(2)
    condition = f'authrequestdate = \'{year}-{month}-{day}\''
    filename = f'{BUCKET_NAME}-{day}-{month}-{year}.csv'
    print("Running Daily mode at dates {}-{}-{}".format(year, month, day))
else:
    today = today - dateutil.relativedelta.relativedelta(months=1)
    day = str(today.day).zfill(2)
    month = str(today.month).zfill(2)
    year = str(today.year).zfill(2)
    lastDay = calendar.monthrange(today.year, today.month)[1]
    condition = f'authrequestdate BETWEEN \'{year}-{month}-1\' AND \'{year}-{month}-{lastDay}\''
    filename = f'{BUCKET_NAME}-{month}-{year}.csv'
    print("Running Monthly mode at dates {}-{}".format(year, month))


cnx = redshift_connector.connect(
    host=DB_HOST,
    user=DB_USER,
    password=DB_PASSWORD,
    database=DB_DATABASE
)
cursor = cnx.cursor()

query = """SELECT
    to_char(authrequestdate, 'DD/MM/YYYY HH24:mi:SS') AS requisicao_da_autorizacao,
    req_external_id AS externalid,
    req_msisdn AS msisdn,
    valor AS amount,
    to_char(resp_time, 'DD/MM/YYYY HH24:mi:SS') AS resposta_da_autorizacao,
    resp_authorized AS authorized,
    nsu,
    response,
    to_char(notify_time, 'DD/MM/YYYY HH24:mi:SS') AS notificacao,
    notify_confirmed AS confirmed,
    operadora
FROM
    refined_benefit.plataforma_recargas_transacao_3meses
WHERE
    {0}
    AND service_id IN ({1})""".format(condition, SERVICES_IDS)

print("Executando consulta...")
print("Query: {}".format(query))
print("Bucket: {}".format(BUCKET_NAME))
print("File: {}".format(filename))
cursor.execute(query)
rows = cursor.fetchall()
cursor.close()
cnx.close()


with open('/tmp/' + filename, 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=';')
    spamwriter.writerow(
        [
            "requisição_da_autorização",
            "externalid",
            "msisdn",
            "amount",
            "resposta_da_autorização",
            "authorized",
            "nsu",
            "response",
            "notificação",
            "confirmed",
            "operadora"
        ]
    )

    print("Found {} rows".format(len(rows)))
    for row in rows:
        spamwriter.writerow(
            [
                row[0],
                row[1],
                row[2],
                row[3],
                row[4],
                row[5],
                row[6],
                row[7],
                row[8],
                row[9],
                row[10]
            ]
        )

print("Fazendo upload...")
print("File: {}".format(filename))
with open(f'/tmp/{filename}', 'rb') as csvfile:
    s3 = boto3.resource('s3')
    key = f'{DIR_NAME}/{filename}'
    print("Uploading file {} to bucket {}".format(filename, BUCKET_NAME))
    s3.Bucket(BUCKET_NAME).put_object(Key=key, Body=csvfile)

print("Relatorio gerado com sucesso!")
