FROM python:3
ADD requirements.txt /
RUN pip install -r requirements.txt
WORKDIR /app
ADD main.py /app/
CMD [ "python", "/app/main.py" ]