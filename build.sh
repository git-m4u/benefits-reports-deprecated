#!/bin/bash

PROJ_PATH=`pwd`
python3 -m venv env
. env/bin/activate
pip install -r requirements.txt
deactivate
cd env/lib/python3.*/site-packages
zip -r $PROJ_PATH/report-deployment-package.zip .
cd $PROJ_PATH
zip -g report-deployment-package.zip lambda_function.py
