#######################################################
# Project info: 
locals {

  app_name          = "benefit-reports"
  team              = "platform"
  responsible_email = data.terraform_remote_state.ecs.outputs.responsible_email

}

#######################################################
# App Settings:
locals {

  app_port    = "8080"
  socket_port = "8000"
  app_cpu     = "512"
  app_mem     = "1024"
  app_image   = "m4ucorp/benefits-reports:${var.commit}"

  # Healthcheck:
  healthcheck_protocol             = "HTTP"
  healthcheck_path                 = "/actuator/health"
  healthcheck_port                 = local.app_port
  healthcheck_matcher              = "200,301"
  healthcheck_interval             = "20"
  healthcheck_grace_period_seconds = "30"
  healthcheck_timeout              = "5"
  healthcheck_healthy_threshold    = "6"
  healthcheck_unhealthy_threshold  = "6"

  # Scaling parameters can be defined by environment:
  mem_target_values         = { default = "70" }
  cpu_target_values         = { default = "70" }
  min_capacity_values       = { default = "1", prd = "2" }
  max_capacity_values       = { default = "1", prd = "2" }
  desired_capacity_values   = { default = "1", prd = "2" }
  scale_in_cooldown_values  = { default = "20", prd = "120" }
  scale_out_cooldown_values = { default = "30", prd = "30" }

  # Cloudwatch log retention
  cloudwatch_log_retention_in_days_values = { default = "5", prd = "30" }

}

output "app_name" { value = local.app_name }
output "team" { value = local.team }
output "responsible_email" { value = local.responsible_email }

#######################################################
# Tags:
locals {

  provider_tags = {
    terraform         = "true"
    business_unit     = local.business_unit
    team              = local.team
    app_name          = local.app_name
    responsible_email = local.responsible_email
    repository        = local.repository
    env               = local.env
  }

  tags = {
    terraform           = "true"
    business_unit       = local.business_unit
    team                = local.team
    app_name            = local.app_name
    responsible_email   = local.responsible_email
    repository          = local.repository
    env                 = local.env
    deployer_account_id = local.deployer_account_id
    deployer_user_id    = local.deployer_user_id
  }

}


#######################################################
# Local calculations:
locals {

  # Some local translation
  env                 = terraform.workspace
  deployer_account_id = data.aws_caller_identity.current.account_id
  deployer_user_id    = split(":", data.aws_caller_identity.current.user_id)[0]
  deployer_arn        = data.aws_caller_identity.current.arn

  commit     = var.commit
  repository = "https://bitbucket.org/${var.repository}"

}

output "env" { value = local.env }
output "deployer_account_id" { value = local.deployer_account_id }
output "deployer_user_id" { value = local.deployer_user_id }
output "repository" { value = local.repository }

#######################################################


# From this point below there are some local calculations
# and remote state data fetch you probably won't need to edit.

#######################################################
# App settings default calculations: 
locals {

  # Environment customizable vars should go here to get the default value:
  mem_target_value                       = try(local.mem_target_values[local.env], local.mem_target_values["default"])
  cpu_target_value                       = try(local.cpu_target_values[local.env], local.cpu_target_values["default"])
  min_capacity_value                     = try(local.min_capacity_values[local.env], local.min_capacity_values["default"])
  max_capacity_value                     = try(local.max_capacity_values[local.env], local.max_capacity_values["default"])
  desired_capacity_value                 = try(local.desired_capacity_values[local.env], local.desired_capacity_values["default"])
  scale_in_cooldown_value                = try(local.scale_in_cooldown_values[local.env], local.scale_in_cooldown_values["default"])
  scale_out_cooldown_value               = try(local.scale_out_cooldown_values[local.env], local.scale_out_cooldown_values["default"])
  cloudwatch_log_retention_in_days_value = try(local.cloudwatch_log_retention_in_days_values[local.env], local.cloudwatch_log_retention_in_days_values["default"])
}


#######################################################
# Remote state from ECS:
locals {

  # Remote state data from ECS Cluster:
  business_unit  = data.terraform_remote_state.ecs.outputs.business_unit
  aws_account_id = data.terraform_remote_state.ecs.outputs.aws_account_id
  region         = data.terraform_remote_state.ecs.outputs.region
  azs            = data.terraform_remote_state.ecs.outputs.azs
  azcount        = data.terraform_remote_state.ecs.outputs.azcount

  vpc_id             = data.terraform_remote_state.ecs.outputs.vpc_id
  vpc_cidr_block     = data.terraform_remote_state.ecs.outputs.vpc_cidr_block
  subnet_private1_id = data.terraform_remote_state.ecs.outputs.subnet_private1_id
  subnet_private2_id = data.terraform_remote_state.ecs.outputs.subnet_private2_id
  subnet_public_id   = data.terraform_remote_state.ecs.outputs.subnet_public_id

  nlb_arn          = data.terraform_remote_state.ecs.outputs.nlb_arn
  listener80_arn   = data.terraform_remote_state.ecs.outputs.listener80_arn
  ecs_cluster_name = data.terraform_remote_state.ecs.outputs.ecs_cluster_name
  ecs_cluster_arn  = data.terraform_remote_state.ecs.outputs.ecs_cluster_arn
  alb_arn          = data.terraform_remote_state.ecs.outputs.alb_arn
  alb_arn_suffix   = data.terraform_remote_state.ecs.outputs.alb_arn_suffix

  alerts_sns_topic_arn = data.terraform_remote_state.ecs.outputs.alerts_sns_topic_arn

}


