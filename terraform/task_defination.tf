module "container_definitions" {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "0.58.0"

  container_name   = local.app_name
  container_image  = local.app_image
  container_cpu    = local.app_cpu
  container_memory = local.app_mem
  port_mappings    = [
    {
      containerPort = local.app_port,
      hostPort      = local.app_port,
      protocol      = "tcp"
    }
  ]

  repository_credentials = {
    credentialsParameter = data.terraform_remote_state.ecs.outputs.sm_docker_credentials_arn
  }

  log_configuration = {
    logDriver = "awslogs",
    options   = {
      "awslogs-create-group"  = "true",
      "awslogs-group"         = aws_cloudwatch_log_group.lg.name,
      "awslogs-region"        = local.region,
      "awslogs-stream-prefix" = "ecs"
    }
  }

  map_environment = {
    DB_PASSWORD = var.DB_PASSWORD
  }
}

resource "aws_ecs_task_definition" "definition" {
  family                   = local.app_name
  container_definitions    = module.container_definitions.json_map_encoded_list
  task_role_arn            = aws_iam_role.execution.arn
  execution_role_arn       = aws_iam_role.execution.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = local.app_cpu
  memory                   = local.app_mem
}

