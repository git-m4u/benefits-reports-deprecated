resource "aws_security_group" "sg" {
  name        = "${local.app_name}-sg"
  description = "${local.app_name}-sg"
  vpc_id      = local.vpc_id
  ingress {
    from_port   = local.app_port
    to_port     = local.app_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(local.tags, { Name = "${local.app_name}-sg" }, )
}
