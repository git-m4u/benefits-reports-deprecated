resource "aws_cloudwatch_log_group" "lg" {
  name              = "ecs-${local.app_name}"
  retention_in_days = 5
  tags              = merge(local.tags, { Name = "ecs-${local.team}-${local.app_name}" }, )
}
