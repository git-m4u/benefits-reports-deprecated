resource "aws_cloudwatch_event_rule" "livelo_report_daily_job" {
  name                = "livelo_report_daily_job"
  description         = "Fires once time a day at 15:00 for livelo report"
  schedule_expression = "cron(30 15 * * ? *)"
}

resource "aws_cloudwatch_event_target" "livelo_report_daily_job" {
  target_id = local.app_name
  arn       = aws_ecs_cluster.cluster.arn
  rule      = aws_cloudwatch_event_rule.livelo_report_daily_job.name
  role_arn  = aws_iam_role.cloudwatch_events_role.arn
  ecs_target {
    task_count          = 1
    task_definition_arn = aws_ecs_task_definition.definition.arn
    launch_type         = "FARGATE"
    network_configuration {
      security_groups = [
        aws_security_group.sg.id
      ]
      subnets         = local.subnet_private1_id
    }
  }
  input = <<DOC
    {
    "containerOverrides": [
      {
        "name": "${local.app_name}",
        "environment": [
          {
            "name": "DAILY_MODE",
            "value": "true"
          },
          {
            "name": "SERVICES_IDS",
            "value": "1801, 1802, 1803, 1804"
          },
          {
            "name": "BUCKET_NAME",
            "value": "livelo-report"
          }
        ]
      }
    ]
  }
DOC
}