resource "aws_iam_role" "execution" {
  name = "${local.app_name}-execution-role"
  tags = merge(local.tags, { Name = "${local.app_name}-execution-role" }, )
  assume_role_policy = data.aws_iam_policy_document.ecs_assume_role_policy.json
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  version = "2012-10-17"
  statement {
    sid = ""
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "execution" {
  name        = "${local.app_name}-execution-policy"
  description = "${local.app_name}-execution-policy"
  policy      = data.aws_iam_policy_document.ecs_execution_role_policy.json
}

data "aws_iam_policy_document" "ecs_execution_role_policy" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:PutLogEvents",
      "logs:CreateLogStream"
    ]
    resources = [
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.lg.name}:*",
      "arn:aws:logs:*:*:log-group:${aws_cloudwatch_log_group.lg.name}:log-stream:*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "secretsmanager:GetSecretValue"
    ]

    resources = [
      "arn:aws:secretsmanager:*:*:secret:*",
      "arn:aws:kms:*:*:*"
    ]
  }

  statement {
    effect   = "Allow"
    actions = [
      "s3:*",
    ]
    resources = ["*"]
  }

}

resource "aws_iam_role_policy_attachment" "execution" {
  role       = aws_iam_role.execution.name
  policy_arn = aws_iam_policy.execution.arn
}

resource "aws_iam_role" "cloudwatch_events_role" {
  name               = "${local.app_name}-events"
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_events_role_assume_policy.json
}

data "aws_iam_policy_document" "cloudwatch_events_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy" "cloudwatch_events_role_run_task" {
  name   = "${aws_ecs_task_definition.definition.family}-events-ecs"
  role   = aws_iam_role.cloudwatch_events_role.id
  policy = data.aws_iam_policy_document.cloudwatch_events_role_run_task_policy.json
}

data "aws_iam_policy_document" "cloudwatch_events_role_run_task_policy" {
  statement {
    effect    = "Allow"
    actions   = ["ecs:RunTask"]
    resources = ["arn:aws:ecs:${local.region}:${data.aws_caller_identity.current.account_id}:task-definition/${aws_ecs_task_definition.definition.family}:*"]

    condition {
      test     = "StringLike"
      variable = "ecs:cluster"
      values   = [aws_ecs_cluster.cluster.arn]
    }
  }
}

resource "aws_iam_role_policy" "cloudwatch_events_role_pass_role" {
  name   = "${aws_ecs_task_definition.definition.family}-events-ecs-pass-role"
  role   = aws_iam_role.cloudwatch_events_role.id
  policy = data.aws_iam_policy_document.cloudwatch_events_role_pass_role_policy.json
}

data "aws_iam_policy_document" "cloudwatch_events_role_pass_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["iam:PassRole"]

    resources = [
      aws_iam_role.execution.arn,
    ]
  }
}