resource "aws_cloudwatch_event_rule" "dotz_report_monthly_job" {
  name                = "dotz_report_monthly_job"
  description         = "Fires once time a day at 15:00 for dotz report"
  schedule_expression = "cron(0 9 1 * ? *)"
}

resource "aws_cloudwatch_event_target" "dotz_report_monthly_job" {
  target_id = local.app_name
  arn       = aws_ecs_cluster.cluster.arn
  rule      = aws_cloudwatch_event_rule.dotz_report_monthly_job.name
  role_arn  = aws_iam_role.cloudwatch_events_role.arn
  ecs_target {
    task_count          = 1
    task_definition_arn = aws_ecs_task_definition.definition.arn
    launch_type         = "FARGATE"
    network_configuration {
      security_groups = [
        aws_security_group.sg.id
      ]
      subnets         = local.subnet_private1_id
    }
  }
  input = <<DOC
    {
    "containerOverrides": [
      {
        "name": "${local.app_name}",
        "environment": [
          {
            "name": "DAILY_MODE",
            "value": "false"
          },
          {
            "name": "SERVICES_IDS",
            "value": "1851, 1852, 1853, 1854"
          },
          {
            "name": "BUCKET_NAME",
            "value": "dotz-report"
          }
        ]
      }
    ]
  }
DOC
}