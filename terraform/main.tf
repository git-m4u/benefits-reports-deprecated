# Providers:
provider "aws" {
  region = local.region
  #  default_tags { tags = local.provider_tags }
  assume_role { role_arn = "arn:aws:iam::${local.aws_account_id}:role/deployer-role" }
}
data "aws_caller_identity" "current" {}


# Terraform:
terraform {
  required_version = ">= 0.15"
  required_providers { aws = { version = ">= 3.36.0" } }
  backend "s3" {
    bucket = "m4u-bu-benefits-terraform-states"
    region = "sa-east-1"
    acl    = "bucket-owner-full-control"
    key    = "lambdas/reports/dotz.state"
  }
}


# Remote state from ECS:
data "terraform_remote_state" "ecs" {
  backend   = "s3"
  workspace = terraform.workspace
  config = {
    bucket = "m4u-bu-benefits-terraform-states"
    region = "sa-east-1"
    key    = "benefits/infra/ecs-cluster.state"
  }
}

